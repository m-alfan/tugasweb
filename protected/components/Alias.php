<?php

class Alias extends CController
{
	//for user management
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;

	public static function TypeAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => '<span class="label label-danger">Not Active</span>',
				self::STATUS_ACTIVE   => '<span class="label label-success">Active</span>',
				self::STATUS_BANNED   => '<span class="label label-warning">Banned</span>',
			),
			'Gender' => array(
				'm' => 'Male',
				'f' => 'Female'
			),
			'StatusNikah' => array(
				'm' => 'Married',
				's' => 'Singgel'
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
}