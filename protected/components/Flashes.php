<?php
class Flashes extends CWidget {
 
    public function run() {
		
		Yii::app()->clientScript->registerScript(
		   'myHideEffect',
		  	 '$(".flashes").animate({opacity: 0.9}, 3500).fadeOut("slow");',
		   CClientScript::POS_READY
		);

		$flashMessages = Yii::app()->user->getFlashes();
		if ($flashMessages) {
			echo '<div class="flashes">';
			foreach($flashMessages as $key => $message) {
				echo '<div class="alert in fade alert-'.$key.'">'.$message . "</div>\n";
			}
			echo '</div>';
		}	
    }
}
?>