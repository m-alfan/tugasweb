<?php

class Controller extends CController
{
	public $layout='//layouts/column1';

	public $menu=array();

	public $breadcrumbs=array();

	//function debug
	public function debugCode($data)
	{
		print "<pre>";
		print_r($data);
		print "</pre>";

		Yii::app()->end();
	}

	public function debugTipe($data)
	{
		print "<pre>";
		var_dump($data);
		print "</pre>";

		Yii::app()->end();
	}

	public function debug($data)
	{
		print "<pre>";
		print_r($data);
		print "</pre>";
	}

	public function end()
	{
		Yii::app()->end();
	}

	public function title()
	{
		$title =  ucwords(Yii::app()->controller->id).' | '.Yii::app()->name;
		return $title;
	}

	public function title_id()
	{
		$title_id =  ucwords(Yii::app()->controller->id).' - '.
				  ucwords(Yii::app()->controller->action->id).' | '.Yii::app()->name;
		return $title_id;
	}

	public function number()
	{
		$data = array(
			'headerHtmlOptions' =>array('style'=>'text-align:center;'),
            'header' => '#',
            'value'  => '($row+1) + ($this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize)',
            'htmlOptions'=>array('width'=>'5%')
        );

        return $data;
	}
}