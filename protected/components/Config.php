<?php

class Config
{
	public static function initApp()
	{
		$_siteConfig = ConfigWeb::model()->findByPk(array('id'=>1));
		// foreach($siteConfig as $conf)
		// {
		// 	define($conf->config_key, $conf->config_value);
		// }

	 	//Yii::app()->theme = 'THEME_COLOR';
		Yii::app()->name = $_siteConfig->name;
		Yii::app()->params->desc    = $_siteConfig->descr;
		Yii::app()->params->company = $_siteConfig->company;
		Yii::app()->params->web 	= $_siteConfig->web;
	}
}