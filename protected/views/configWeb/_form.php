<div class="row">
	<div class="col-md-12 column">
		<?php $form = $this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'	=>'config-web-form',
			'enableAjaxValidation' => TRUE,
		)); ?>

		<hr>
		<p class="help-block">
			<h5>Fields with <span class="required">*</span> are required.</h5>
		</p>

		<?= $form->errorSummary($model); ?>

		<?= $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>100)))); ?>

		<?= $form->textAreaGroup($model,'descr', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50)))); ?>

		<?= $form->textFieldGroup($model,'company',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>45)))); ?>

		<?= $form->textFieldGroup($model,'web',array('widgetOptions'=>array('htmlOptions'=>array('maxlength'=>45)))); ?>

		<div class="form-actions">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'	 => 'primary',
					'label'	 	 => $model->isNewRecord ? 'Create' : 'Save',
			)); ?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>