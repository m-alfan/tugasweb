<?php
$this->breadcrumbs=array(
		'Configuration'=>array('index'),
		'Edit',
	);
?>

<h1>Configuration of Application</h1>

<?= $this->renderPartial('_form',array('model'=>$model)); ?>