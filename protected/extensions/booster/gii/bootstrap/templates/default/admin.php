<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
	echo "<?php\n";
	$label = $this->class2name($this->modelClass);
	echo "\$this->breadcrumbs=array(
		'$label'=>array('admin'),
		'Manage',
	);\n";
?>

	$this->menu=array(
		array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
		array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin')),
	);

	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
				data: $(this).serialize()
			});
			return false;
		});
	");
?>

<h1>Manage <?php echo $this->class2name($this->modelClass); ?></h1>

<?php echo "<?= CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>"; ?>

<div class="search-form" style="display:none">
	<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?> $this->widget('booster.widgets.TbGridView',array(
	'id'		   => '<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider' => $model->search(),
	'filter'	   => $model,
	'columns'	   => array(
<?php
$count = 0;
foreach ($this->tableSchema->columns as $column) {
	if (++$count == 7) {
		echo "\t\t/*\n";
	}
	echo "\t\t'" . $column->name . "',\n";
}
if ($count >= 7) {
	echo "\t\t*/\n";
}
?>
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
