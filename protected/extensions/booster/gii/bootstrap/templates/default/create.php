<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
	$label = $this->class2name($this->modelClass);
	echo "\$this->breadcrumbs = array(
		'$label' => array('admin'),
		'Create',
	);\n";
?>

	$this->menu=array(
		array('label' => 'Create <?php echo $this->modelClass; ?>','url' => array('create')),
		array('label' => 'Manage <?php echo $this->modelClass; ?>','url' => array('admin')),
	);
?>

<h1>Create <?php echo $this->modelClass; ?></h1>

<?php echo "<?= \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
