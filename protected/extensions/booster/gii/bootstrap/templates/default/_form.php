<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<div class="row">
	<div class="col-md-12 column">
		<?php echo "<?php \$form = \$this->beginWidget('booster.widgets.TbActiveForm',array(
			'id'	=>'" . $this->class2id($this->modelClass) . "-form',
			'enableAjaxValidation' => TRUE,
		)); ?>\n"; ?>

		<hr>
		<p class="help-block">
			<h5>Fields with <span class="required">*</span> are required.</h5>
		</p>

		<?php echo "<?= \$form->errorSummary(\$model); ?>\n"; ?>

<?php
foreach ($this->tableSchema->columns as $column) {
	if ($column->autoIncrement) {
		continue;
	}
	?>
		<?php echo "<?= " . $this->generateActiveGroup($this->modelClass, $column) . "; ?>\n"; ?>

<?php
}
?>
		<div class="form-actions">
			<?php echo "<?php \$this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'	 => 'primary',
					'label'	 	 => \$model->isNewRecord ? 'Create' : 'Save',
			)); ?>\n"; ?>
		</div>

		<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
	</div>
</div>