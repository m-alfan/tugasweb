<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->class2name($this->modelClass);
	echo "\$this->breadcrumbs = array(
		'$label' => array('admin'),
		\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
		'Edit',
	);\n";
?>

	$this->menu=array(
		array('label' => 'Create <?php echo $this->modelClass; ?>','url' => array('create')),
		array('label' => 'View <?php echo $this->modelClass; ?>','url' => array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
		array('label' => 'Manage <?php echo $this->modelClass; ?>','url' => array('admin')),
	);
?>

<h1>Edit <?php echo $this->modelClass . " <?= \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?= \$this->renderPartial('_form',array('model'=>\$model)); ?>"; ?>