<?php

class Profile extends CActiveRecord
{
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableProfiles;
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name, no_ktp, place_birth', 'length', 'max'=>30),
			array('npwp, jabatan, pendidikan', 'length', 'max'=>45),
			array('no_telp, no_hp', 'length', 'max'=>20),
			array('status_nikah, jk', 'length', 'max'=>1),
			array('photo','file', 'types' => 'jpg,jpeg,png,gif', 'maxSize' => 512000, 'allowEmpty' => TRUE), //max 500kb, 1 kb = 1024 byte
			array('address, date_birth, date_start, date_end', 'safe'),
			array('user_id, name, address, no_telp, no_hp, no_ktp, jk, place_birth, date_birth', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		$relations = array(
			'user'=>array(self::HAS_ONE, 'User', 'id'),
		);
		if (isset(Yii::app()->getModule('user')->profileRelations)) $relations = array_merge($relations,Yii::app()->getModule('user')->profileRelations);
		return $relations;
	}

	public function attributeLabels()
	{
		return array(
			'user_id' 	  => 'ID',
			'name'   	  => 'Name',
			'address' 	  => 'Address',
			'no_telp' 	  => 'No Telpon',
			'no_hp' 	  => 'No Handphone',
			'no_ktp' 	  => 'No KTP',
			'jk' 		  => 'Gender',
			'place_birth' => 'Place of Birth',
			'date_birth'  => 'Date of Birth',
			'photo'		  => 'Photo',
			'npwp'        => 'NPWP',
			'status_nikah'=> 'Status Nikah',
			'pendidikan'  => 'Education',
			'jabatan'     => 'Occupation',
			'date_start'  => 'Start Work',
			'date_end'    => 'Resign'
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('no_telp',$this->no_telp,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('no_ktp',$this->no_ktp,true);
		$criteria->compare('jk',$this->jk,true);
		$criteria->compare('place_birth',$this->place_birth,true);
		$criteria->compare('date_birth',$this->date_birth,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('status_nikah',$this->status_nikah,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}