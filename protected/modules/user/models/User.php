<?php

class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;
	
	const STATUS_BANED=-1;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	public function rules()
	{
		return array(
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANNED)),
            array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
            array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			array('username, password, email, status', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('id, nameProfile, username, password, email, activkey, create_at, lastvisit_at, status', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
        $relations = Yii::app()->getModule('user')->relations;
        if (!isset($relations['profile']))
            $relations['profile'] 		= array(self::HAS_ONE, 'Profile', 'user_id');
        return $relations;
	}

	public function attributeLabels()
	{
		return array(
			'id' 			 => UserModule::t("ID"),
			'username'		 => UserModule::t("Username"),
			'password'		 => UserModule::t("Password"),
			'verifyPassword' => UserModule::t("Retype Password"),
			'email'			 => UserModule::t("E-mail"),
			'verifyCode'	 => UserModule::t("Verification Code"),
			'activkey' 		 => UserModule::t("Activation Key"),
			'createtime' 	 => UserModule::t("Registration Date"),
			'create_at' 	 => UserModule::t("Registration Date"),
			'lastvisit_at' 	 => UserModule::t("Last visit"),
			'status' 		 => UserModule::t("Status")
		);
	}
	
	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANNED,
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, status',
            ),
        );
    }
	
	public function defaultScope()
    {
        return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope,array(
            'alias'=>'user',
            'select' => 'user.id, user.username, user.email, user.create_at, user.lastvisit_at, user.status',
        ));
    }

    public function search()
    {
        $criteria = new CDbCriteria;
        
        $criteria->compare('user.id',$this->id);
        $criteria->compare('user.username',$this->username,true);
        //$criteria->compare('user.password',$this->password);
        $criteria->compare('user.email',$this->email,true);
        //$criteria->compare('user.activkey',$this->activkey);
        //$criteria->compare('user.create_at',$this->create_at);
        $criteria->compare('user.lastvisit_at',$this->lastvisit_at);
        //$criteria->compare('user.status',$this->status);

        //for name
        $criteria->compare('profile.name',$this->nameProfile);

        $sort = new CSort();
        $sort->attributes = array(
            'defaultOrder'=>'user.id DESC',

            'username'=>array(
                'asc'=>'user.username',
                'desc'=>'user.username desc',
            ),

            'email'=>array(
                'asc'=>'user.email',
                'desc'=>'user.email desc',
            ),

            'lastvisit_at'=>array(
                'asc'=>'user.lastvisit_at',
                'desc'=>'user.lastvisit_at desc',
            ),

            'nameProfile'=>array(
                'asc'=>'profile.name',
                'desc'=>'profile.name desc',
            ),
        );

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
            'sort'    => $sort,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('user')->user_page_size,
			),
        ));
    }

    public function getCreatetime() {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value) {
        $this->create_at=date('Y-m-d H:i:s',$value);
    }

    public function getLastvisit() {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value) {
        $this->lastvisit_at=date('Y-m-d H:i:s',$value);
    }

    private $_nameProfile = null;

    public function getnameProfile()
    {
        if ($this->_nameProfile === null && $this->profile !== null)
        {
            $this->_nameProfile = $this->profile->name;
        }
        return $this->_nameProfile;
    }
    
    public function setnameProfile($value)
    {
        $this->_nameProfile = $value;
    }

}