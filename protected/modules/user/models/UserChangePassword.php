<?php
class UserChangePassword extends CFormModel {
	public $oldPassword;
	public $password;
	public $verifyPassword;
	
	public function rules() {
		return array(
			array('oldPassword, password, verifyPassword', 'required'),
			array('oldPassword, password, verifyPassword', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")),
			array('oldPassword', 'verifyOldPassword'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'oldPassword'	 => UserModule::t("Old Password"),
			'password'		 => UserModule::t("Password"),
			'verifyPassword' => UserModule::t("Retype Password"),
		);
	}
	
	public function verifyOldPassword($attribute, $params)
	{
		if (User::model()->notsafe()->findByPk(Yii::app()->user->id)->password != Yii::app()->getModule('user')->encrypting($this->$attribute))
			$this->addError($attribute, UserModule::t("Old Password is incorrect."));
	}
}