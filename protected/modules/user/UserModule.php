<?php

class UserModule extends CWebModule
{
	public $user_page_size 		= 10;
	
	public $fields_page_size 	= 10;
	
	public $hash 			 	= 'md5';

	public $sendActivationMail	= true;

	public $loginNotActiv 		= false;

	public $activeAfterRegister	= false;
	
	public $autoLogin	 		= true;
	
	public $loginUrl 		= array("/user/login");
	public $logoutUrl 		= array("/user/logout");
	public $profileUrl		= array("/user/profile");
	public $returnUrl 		= array("/user/profile");
	public $returnLogoutUrl = array("/user/login");
	
	public $rememberMeTime  = 2592000; // 30 days
	
	public $fieldsMessage 	= '';
	
	public $relations 		= array();

	public $profileRelations= array();
	
	//public $cacheEnable = false;

	public $tableUsers		= 'users';
	public $tableProfiles 	= 'profiles';

    public $defaultScope 	= array(
            'with'=>array('profile'),
    );
	
	static private $_user;
	static private $_users 		= array();
	static private $_userByName = array();
	
	public $componentBehaviors	= array();
	
	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'user.models.*',
			'user.components.*',
		));
	}
	
	public function getBehaviorsFor($componentName){
        if (isset($this->componentBehaviors[$componentName])) {
            return $this->componentBehaviors[$componentName];
        } else {
            return array();
        }
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	
	public static function t($str='',$params=array(),$dic='user') {
		if (Yii::t("UserModule", $str)==$str)
		    return Yii::t("UserModule.".$dic, $str, $params);
        else
            return Yii::t("UserModule", $str, $params);
	}
	
	public static function encrypting($string="") {
		$hash = Yii::app()->getModule('user')->hash;
		if ($hash=="md5")
			return md5($string);
		if ($hash=="sha1")
			return sha1($string);
		else
			return hash($hash,$string);
	}
	
	public static function doCaptcha($place = '') {
		if(!extension_loaded('gd'))
			return false;
		if (in_array($place, Yii::app()->getModule('user')->captcha))
			return Yii::app()->getModule('user')->captcha[$place];
		return false;
	}

	public static function sendMail($email,$subject,$message) {
    	$adminEmail = Yii::app()->params['adminEmail'];
	    $headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
	    $message = wordwrap($message, 70);
	    $message = str_replace("\n.", "\n..", $message);
	    return mail($email,'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);
	}
	
	public static function user($id=0,$clearCache=false) {
        if (!$id&&!Yii::app()->user->isGuest)
            $id = Yii::app()->user->id;
		if ($id) {
            if (!isset(self::$_users[$id])||$clearCache)
                self::$_users[$id] = User::model()->with(array('profile'))->findbyPk($id);
			return self::$_users[$id];
        } else return false;
	}
	
	public static function getUserByName($username) {
		if (!isset(self::$_userByName[$username])) {
			$_userByName[$username] = User::model()->findByAttributes(array('username'=>$username));
		}
		return $_userByName[$username];
	}

	public function users() {
		return User;
	}
}
