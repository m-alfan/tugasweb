<?php

class AdminController extends Controller
{
	public $defaultAction = 'admin';
	public $layout 		  = '//layouts/column2';
	
	private $_model;

	public function filters()
	{
		return CMap::mergeArray(parent::filters(),array(
			'accessControl', // perform access control for CRUD operations
		));
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('admin','delete','create','update','view'),
				'users'=> array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionAdmin()
	{
		$this->pageTitle = $this->title_id();

		$model = new User('search');
        $model->unsetAttributes();
        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];

        $this->render('index',array(
            'model'=>$model,
        ));
	}

	public function actionView()
	{
		$this->pageTitle = $this->title_id();

		$model = $this->loadModel();
		$this->render('view',array(
			'model'=> $model,
		));
	}

	public function actionCreate()
	{
		$this->pageTitle = $this->title_id();

		$model   = new User;
		$profile = new Profile;

		$this->performAjaxValidation(array($model,$profile));

		if(isset($_POST['User']))
		{
			$model->attributes   = $_POST['User'];
			$model->activkey     = Yii::app()->controller->module->encrypting(microtime().$model->password);
			$profile->attributes = $_POST['Profile'];
			$profile->user_id    = 0;
			
			if($model->validate() && $profile->validate()) {
				$model->password  = Yii::app()->controller->module->encrypting($model->password);
				if($model->save()) {
					$profile->user_id = $model->id;

					//upload photo
					$upload  = CUploadedFile::getInstance($profile,'photo');
					if(!empty($upload))
					{
						$name     = $_FILES['Profile']['name']['photo'];
						//$filename = pathinfo($name, PATHINFO_FILENAME);
						$ext      = pathinfo($name, PATHINFO_EXTENSION);
						$new_name = date("Y-m-d-h-i",time())."-".$profile->user_id.'.'.$ext;

						$upload->saveAs(Yii::app()->basePath.'/../images/user/'.$new_name);
						$profile->photo = $new_name;
					}

					$profile->save();
				}
				Yii::app()->user->setFlash('success',UserModule::t("New employee is saved."));
				$this->redirect(array('view','id'=>$model->id));
			} else {
				Yii::app()->user->setFlash('danger',UserModule::t("Error when update employee."));
				$profile->validate();
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

	public function actionUpdate()
	{
		$this->pageTitle = $this->title_id();
		
		$model   = $this->loadModel();
		$profile = $model->profile;
		$this->performAjaxValidation(array($model,$profile));

		if(isset($_POST['User']))
		{
			$model->attributes   = $_POST['User'];
			$profile->attributes = $_POST['Profile'];
			
			if($model->validate()&&$profile->validate()) {
				$old_password = User::model()->notsafe()->findByPk($model->id);
				if ($old_password->password!=$model->password) {
					$model->password=Yii::app()->controller->module->encrypting($model->password);
					$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
				}
				//upload photo
				$upload  = CUploadedFile::getInstance($profile,'photo');
				if(!empty($upload))
				{
					$name     = $_FILES['Profile']['name']['photo'];
					//$filename = pathinfo($name, PATHINFO_FILENAME);
					$ext      = pathinfo($name, PATHINFO_EXTENSION);
					$new_name = date("Y-m-d-h-i",time())."-".$profile->user_id.'.'.$ext;

					$upload->saveAs(Yii::app()->basePath.'/../images/user/'.$new_name);
					$profile->photo = $new_name;
				}
				
				$model->save();
				$profile->save();
				Yii::app()->user->setFlash('success',UserModule::t("Update employee is saved."));
				$this->redirect(array('view','id'=>$model->id));
			} else {
				Yii::app()->user->setFlash('danger',UserModule::t("Error when update employee."));
				$profile->validate();
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model   = $this->loadModel();
			$profile = Profile::model()->findByPk($model->id);
			$profile->delete();
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(array('/user/admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
    protected function performAjaxValidation($validate)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
        {
            echo CActiveForm::validate($validate);
            Yii::app()->end();
        }
    }
	
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model = User::model()->notsafe()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
}