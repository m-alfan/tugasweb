<div class="row">
	<div class="col-md-12 column">
		<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
			'id' 	=>'user-form',
			'enableAjaxValidation'=> true,
			'htmlOptions' 		  => array('enctype'=>'multipart/form-data'),
		));
		?>

		<hr>
		<h3>User ID</h3>
		<p class="help-block">
			<h5>Fields with <span class="required">*</span> are required.</h5>
		</p>

		<?= $form->errorSummary($model); ?>
		<div class="row">
			<div class="col-md-6">
				<?= $form->textFieldGroup($model,'username',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>20)),
					)
				); ?>

				<?= $form->passwordFieldGroup($model,'password',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>128)),
					)
				); ?>
			</div>
			<div class="col-md-6">
				<?= $form->textFieldGroup($model,'email',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>128)),
					)
				); ?>

				<?= $form->dropDownListGroup($model,'status',
					array(
						'widgetOptions'=>array('data'=>array(""=>"Active/Not Active","1"=>"Active","0"=>"Not Active")),
					)
				); ?>
			</div>			
		</div>

		<h3>Profile ID</h3>
		<?= $form->errorSummary($profile); ?>
		<div class="row">
			<div class="col-md-6">
				<?= $form->textFieldGroup($profile,'name',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>30))
					)
				); ?>

				<?= $form->textFieldGroup($profile,'place_birth',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>30))
					)
				); ?>

				<?= $form->datePickerGroup($profile,'date_birth',
					array(
						'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','language'=>'en')),
						'append'=>'<i class="glyphicon glyphicon-calendar"></i>',
					)
				); ?>

				<?= $form->textFieldGroup($profile,'no_telp',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>20))
						)
				); ?>

				<?= $form->textFieldGroup($profile,'no_hp',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>20))
						)
				); ?>

				<?= $form->textFieldGroup($profile,'jabatan',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>45))
						)
				); ?>

				<?= $form->datePickerGroup($profile,'date_start',
					array(
						'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','language'=>'en')),
						'append'=>'<i class="glyphicon glyphicon-calendar"></i>',
					)
				); ?>

				<?= $form->datePickerGroup($profile,'date_end',
					array(
						'widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','language'=>'en')),
						'append'=>'<i class="glyphicon glyphicon-calendar"></i>',
					)
				); ?>
				
			</div>
			<div class="col-md-6">
				<?= $form->textFieldGroup($profile,'no_ktp',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>30))
						)
				); ?>

				<?= $form->radioButtonListGroup($profile,'jk',
					array(
						'widgetOptions' => array('data'=>array("m"=>" Male","f"=>" Female")),
						'inline'		=> TRUE
					)
				); ?>

				<?= $form->textAreaGroup($profile,'address',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8'))
					)
				); ?>

				<?= $form->textFieldGroup($profile,'npwp',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>45))
					)
				); ?>

				<?= $form->radioButtonListGroup($profile,'status_nikah',
					array(
						'widgetOptions' => array('data'=>array("s"=>" Singgel","m"=>" Married")),
						'inline'		=> TRUE
					)
				); ?>

				<?= $form->textFieldGroup($profile,'pendidikan',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>45))
					)
				); ?>

				<?= $form->fileFieldGroup($profile,'photo',
					array(
						'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>45))
					)
				); ?>
			</div>
		</div>

		<div class="form-actions pull-right">		
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'reset',
					'context'	 => 'default',
					'label'	 	 => 'Reset',
			)); ?>

			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType' => 'submit',
					'context'	 => 'primary',
					'label'	 	 => $model->isNewRecord ? 'Create' : 'Save',
			)); ?>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>