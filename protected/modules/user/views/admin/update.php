<?php
	$this->breadcrumbs = array(
		'User' => array('admin'),
		$model->username => array('view','id'=>$model->id),
		'Update'
	);

	$this->menu = array(
		array('label' => 'Create Employee','icon'=>'glyphicon glyphicon-plus','url' => array('create')),
		array('label' => 'View Employee','icon'=>'glyphicon glyphicon-share','url' => array('view','id'=>$model->id)),
		array('label' => 'Delete Employee','icon'=>'glyphicon glyphicon-trash','url' => '#','linkOptions' => array('submit' => array('delete','id'=>$model->id),'confirm' => 'Are you sure you want to delete this item?')),
		array('label' => 'Manage Employees','icon'=>'glyphicon glyphicon-list-alt','url' => array('admin')),
	);
?>

<h1>Update Employee "#<?= $model->username; ?>"</h1>

<?= $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));?>