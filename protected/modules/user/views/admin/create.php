<?php
	$this->breadcrumbs = array(
		'Employee'=>array('admin'),
		'Create',
	);

	$this->menu = array(
		array('label' => 'Create Employee','icon'=>'glyphicon glyphicon-plus','url' => array('create')),
		array('label' => 'Manage Employees','icon' => 'glyphicon glyphicon-list-alt','url' => array('admin')),
	);
?>
<h1>Create Employee</h1>

<?= $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));?>