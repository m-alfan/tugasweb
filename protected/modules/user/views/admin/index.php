<?php
	$this->breadcrumbs = array(
		'Employee'=>array('admin'),
		'Manage',
	);

	$this->menu = array(
		array('label' => 'Create Employee','icon'=>'glyphicon glyphicon-plus','url' => array('create')),
		array('label' => 'Manage Employees','icon' => 'glyphicon glyphicon-list-alt','url' => array('admin')),
	);
?>

<h1>Manage Employees</h1>

<?php $this->widget('booster.widgets.TbGridView', array(
	'id'		   => 'user-grid',
	'dataProvider' => $model->search(),
	'filter'	   => $model,
	'columns'	   => array(
		$this->number(),
		'nameProfile',
		array(
			'name'	=> 'username',
			'type'	=> 'raw',
			'value' => 'CHtml::link(UHtml::markSearch($data,"username"),array("admin/update","id"=>$data->id))',
		),
		'email',
		'lastvisit_at',
		array(
			//'name'	=> 'status',
			'type'  => 'html',
			'value' => 'Alias::TypeAlias("UserStatus",$data->status)',
			'filter'=> Alias::TypeAlias("UserStatus"),
		),
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>