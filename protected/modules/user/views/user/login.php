<div class="panel panel-default shadow">
  <div class="panel-body">
  		<p>
  			<h4>Sign in to your account</h4>
			<hr>
			<h6>Please enter your name and password to log in.</h6>
  		</p>
		<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>
		<?= $form->errorSummary($model); ?>
			<?= $form->textFieldGroup($model, 'username');?>
			<?= $form->passwordFieldGroup($model, 'password');?>
			<?= $form->checkboxGroup($model, 'rememberMe');?>

			<?php $this->widget(
			    'booster.widgets.TbButton',
			    array('buttonType' => 'submit', 
			    	  'label' => 'Login', 
			    	  'htmlOptions' => array('class' => 'btn btn-lg btn-primary btn-block'),
			   	)
			);?>

		<?php $this->endWidget(); ?>
  </div>
</div>