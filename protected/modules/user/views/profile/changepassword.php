<?php
	$this->breadcrumbs = array(
		'Profile' => array('profile'),
		'Change Password'
	);

	$this->menu = array(
		array('label' => 'Profile','icon'=>'glyphicon glyphicon-user','url' => array('profile')),
		array('label' => 'Change Password','icon'=>'glyphicon glyphicon-lock','url' => array('changepassword')),
	);
?>

<h1>Change Password</h1>
<hr>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' 	=>'changepassword-form',
	'enableAjaxValidation'=>true,
	'type'	=> 'horizontal',
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	
	<p class="help-block">
		<h5>Fields with <span class="required">*</span> are required.</h5>
	</p>

	<?= $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-10">
			<?= $form->passwordFieldGroup($model,'oldPassword',
				array(
					'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>128)),
				)
			); ?>
			<hr>
			<?= $form->passwordFieldGroup($model,'password',
				array(
					'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>128)),
					'hint' =>'Minimal password length 4 symbols.'
				)
			); ?>

			<?= $form->passwordFieldGroup($model,'verifyPassword',
				array(
					'widgetOptions'=>array('htmlOptions'=>array('maxlength'=>128)),
				)
			); ?>
			
		</div>
	</div>	
	
	<div class="form-actions">		
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType' => 'reset',
				'context'	 => 'default',
				'label'	 	 => 'Reset',
		)); ?>

		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType' => 'submit',
				'context'	 => 'primary',
				'label'	 	 => 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
