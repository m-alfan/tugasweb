<?php
	$this->breadcrumbs = array(
		'Profile'
	);

	$this->menu = array(
		array('label' => 'Profile','icon'=>'glyphicon glyphicon-user','url' => array('profile')),
		array('label' => 'Change Password','icon'=>'glyphicon glyphicon-lock','url' => array('changepassword')),
	);
?>

<h1>User Profile "#<?= $model->username; ?>"</h1>
<hr>

<div class="row">
	<div class="col-md-4">
	<?php
		$profile = $model->profile;
		$path    = Yii::app()->request->baseUrl.'/images/user/'.$profile->photo;
	?>
	<center>
		<h3><?= '@'.$model->username;?></h3>
		<?= Alias::TypeAlias("UserStatus",$model->status);?>
		<br><br>
		<img src="<?= $path;?>" alt="Tidak Ada Foto" width="200px" height="150px" class="img-thumbnail" >
	</center>
	</div>
	<div class="col-md-8">
		<br>
		<table class="table table-condensed table-hover">
			<thead>
				<tr>
					<th colspan="2">General Information</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$label = array('name','jk','no_ktp','npwp','place_birth','date_birth','status_nikah','pendidikan','jabatan');

					foreach ($label as $key => $value) {
						if($value == 'jk') $profile->$value = Alias::TypeAlias("Gender",$profile->jk);
						echo '
						<tr>
							<td width="30%">'.$profile->getAttributeLabel($value).'</td>
							<td>'.$profile->$value.'</td>
						</tr>';
					}
				?>
			</tbody>
		</table>
		<br>
		<table class="table table-condensed table-hover">
			<thead>
				<tr>
					<th colspan="2">Contact Information</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td width="30%"><?= $model->getAttributeLabel('email'); ?></td>
					<td><?= $model->email;?></td>
				</tr>
				<?php
					$label2 = array('date_start','date_end','no_hp','no_telp','address');

					foreach ($label2 as $key => $value) {
						echo '
						<tr>
							<td width="30%">'.$profile->getAttributeLabel($value).'</td>
							<td>'.$profile->$value.'</td>
						</tr>';
					}
				?>
			</tbody>
		</table>
		<br>
		<table class="table table-condensed table-hover">
			<thead>
				<tr>
					<th colspan="2">Additional information</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$label3 = array('create_at','lastvisit_at');

					foreach ($label3 as $key => $value) {
						echo '
						<tr>
							<td width="30%">'.$model->getAttributeLabel($value).'</td>
							<td>'.$model->$value.'</td>
						</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
</div>