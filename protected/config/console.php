<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		
		'db'=>array(
			'connectionString' => 'mysql:host=127.0.0.1;dbname=app_gpay',
			'emulatePrepare'   => true,
			'username' 		   => 'alfan',
			'password' 		   => 'alfan',
			'charset'  		   => 'utf8',
		),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),

	'modules'=>array(
		'user' => array(
			'hash' 				  => 'md5',
			'sendActivationMail'  => FALSE,
			'loginNotActiv' 	  => FALSE,
			'activeAfterRegister' => FALSE,
			'autoLogin' 		  => TRUE,
			'loginUrl' 			  => array('/user/login'),
			'returnUrl' 		  => array('/user/profile'),
			'returnLogoutUrl' 	  => array('/user/login'),
		)
	),
);