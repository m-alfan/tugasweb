<?php

Yii::setPathOfAlias('booster', dirname(__FILE__) . DIRECTORY_SEPARATOR . '../extensions/booster');

return array(
	'basePath'	=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'		=> 'Application',

	//web config
	'onBeginRequest' => array('Config', 'initApp'),

	'defaultController' => 'home',

	//timezone
    'timeZone' => 'Asia/Jakarta',

	// preloading 'log' component
	'preload'	=> array('log','booster',),

	// autoloading model and component classes
	'import'	=> array(
		'application.models.*',
		'application.components.*',

		//accses user
		'application.modules.user.models.*',
		'application.modules.user.components.*',
	),

	'theme'		=> 'blue',
	'modules'	=> array(
		'gii'	=> array(
			'class'			=> 'system.gii.GiiModule',
			'password'		=> '12345',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'		=> array('127.0.0.1','::1'),
			//booster
			'generatorPaths'=> array(
				'booster.gii',
				),
		),

		//access user
		'user' => array(
			'hash' 				  => 'md5',
			'sendActivationMail'  => FALSE,
			'loginNotActiv' 	  => FALSE,
			'activeAfterRegister' => FALSE,
			'autoLogin' 		  => TRUE,
			'loginUrl' 			  => array('/user/login'),
			'returnUrl' 		  => array('/user/profile'),
			'returnLogoutUrl' 	  => array('/user/login'),
		)
	),

	// application components
	'components'=> array(
		
		'booster' => array(
		    'class' => 'booster.components.Booster',
		),

		'user'=>array(
			'class' 		 => 'WebUser',
			'allowAutoLogin' => TRUE,
			'loginUrl' 		 => array('/user/login'),
		),

		'urlManager' => array(
			'urlFormat'	=> 'path',
			'rules'		=> array(
				'<controller:\w+>/<id:\d+>'				=> '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=> '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'			=> '<controller>/<action>',
			),
			'showScriptName' => FALSE,
		),
		/*
		'db'=> array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/

		'db' => array(
			'connectionString' 	=> 'mysql:host=127.0.0.1;dbname=dev_tugas',
			'emulatePrepare' 	=> TRUE,
			'username' 			=> 'alfan',
			'password' 			=> 'alfan',
			'charset' 			=> 'utf8',
		),
		'errorHandler' => array(
			'errorAction' => 'error',
		),
		
		'log' => array(
			'class'	=> 'CLogRouter',
			'routes'=> array(
				array(
					'class'	=> 'CFileLogRoute',
					'levels'=> 'error, warning',
				),
				/*
				array(
					'class'=> 'CWebLogRoute',
				),
				*/
			),
		),
	),

	'params' => array(
		'adminEmail' => 'webmaster@example.com',
		'desc'		 => 'Description',
		'company'	 => 'Company',
		'web'		 => 'Website',
	),
);