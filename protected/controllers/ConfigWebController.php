<?php

class ConfigWebController extends Controller
{
	public $defaultAction = 'admin';
	
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				  'actions'	=> array('update'),
				  'users'	=> array('@'),
			),
			array('deny',
				  'users'	=> array('*'),
			),
		);
	}

	public function actionUpdate()
	{
		$this->pageTitle = $this->title_id();
		$id    = 1;
		$model = $this->loadModel($id);

		$this->performAjaxValidation($model);

		if(isset($_POST['ConfigWeb']))
		{
			$model->attributes = $_POST['ConfigWeb'];
			if($model->save()) {
				Yii::app()->user->setFlash('success','Update configuration is saved.');
				$this->redirect(array('/home'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model = ConfigWeb::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='config-web-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
