<?php

class HomeController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				  'actions'	=> array('index'),
				  'users'	=> array('@'),
			),
			array('deny',
				  'users'	=> array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$this->pageTitle = $this->title();
		$this->render('index');
	}
}