<?php

class ErrorController extends Controller
{
	public function actionIndex()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionTest()
	{
		$admins = Yii::app()->getModule('user')->isOwner();

		$this->debugTipe($admins);
	}
}