<?php

class ConfigWeb extends CActiveRecord
{
	public function tableName()
	{
		return 'config_web';
	}

	public function rules()
	{
		return array(
			array('name, descr', 'required'),
			array('name', 'length', 'max'=>100),
			array('company, web', 'length', 'max'=>45),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id'   	  => 'ID',
			'name' 	  => 'Name of Application',
			'descr'	  => 'Description',
			'company' => 'Company',
			'web'	  => 'Website'
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
