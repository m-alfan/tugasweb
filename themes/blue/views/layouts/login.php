<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><?= CHtml::encode($this->pageTitle); ?></title>
	<?php 
		$config		= Yii::app();
		$cs 		= Yii::app()->clientScript;
    	$themePath 	= Yii::app()->theme->baseUrl;
		$cs->registerMetaTag(null, null, null, array('charset'=>'UTF-8'))
		   ->registerMetaTag('alfan','author')
		   ->registerCSSFile($themePath.'/css/style.css');
	?>

	<style type="text/css">
		body {
		  	padding-top: 40px;
		  	padding-bottom: 40px;
		}
		.shadow1{
			-webkit-box-shadow: 0 0 5px 2px rgba(0, 0, 0, .5);
        	box-shadow: 0 0 5px 2px rgba(0, 0, 0, .5);
		}
		.shadow { 
			-moz-box-shadow:10px 20px 20px rgba(0,0,0,.5);   
			-webkit-box-shadow:10px 20px 20px rgba(0,0,0,.5);  
			box-shadow: 10px 20px 20px rgba(0,0,0,.5);  
		}  
	</style>
</head>

<body>
	<div class="container">
	    <div class="row">
	        <div class="col-sm-4 col-sm-offset-4">
	        	<h2 align="center"><?= $config->name;?></h2>
				<p align="center"><?= $config->params->desc;?></p>
				<?= $content;?>	            
	        </div>
	    </div>
	    <div class="row">
			<div class="col-sm-4 col-sm-offset-4">
				<hr>
					<center>
						Copyright &copy; <?= date('Y'); ?> by <?= $config->params->company;?>.<br/>
						<?= $config->params->web;?><br/>
						All Rights Reserved.
					</center>
			</div>
		</div>
	</div>
</body>
</html>	