<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><?= CHtml::encode($this->pageTitle); ?></title>
	<?php 
		$config		= Yii::app();
		$cs 		= Yii::app()->clientScript;
    	$themePath 	= Yii::app()->theme->baseUrl;
		$cs->registerMetaTag(null, null, null, array('charset'=>'UTF-8'))
		   ->registerMetaTag('alfan','author')
	       ->registerCSSFile($themePath.'/css/style.css');
	?>

	<script type="text/javascript">
		$(document).ready(function() {
		    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
		    var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
		    var newDate = new Date();
		    newDate.setDate(newDate.getDate());
		    $('#Date').html(dayNames[newDate.getDay()] + ", " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());
		});
	</script>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 column">
				<div class="page-header">
					<h1><?= $config->name;?> <small><?= $config->params->desc;?></small></h1>
					<div class="pull-right">
						<h4><div id="Date"></div></h4>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 column">
				<?php require_once('_menu.php');?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 column">
				<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('booster.widgets.TbBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
				)); ?><!-- breadcrumbs -->
				<?php endif?>

				<?php $this->widget('Flashes'); ?>
			</div>
		</div>
		<div class="row">
				<?= $content; ?>
		</div>
		<div class="row">
			<div class="col-md-12 column">
				<hr>
					<center>
						Copyright &copy; <?= date('Y'); ?> by <?= $config->params->company;?>.<br/>
						<?= $config->params->web;?><br/>
						All Rights Reserved.
					</center>
			</div>
		</div>
	</div>
</body>
</html>
