<?php $this->beginContent('//layouts/main'); ?>
	<div class="col-md-12 column">
		<div class="panel panel-default">
			<div class="panel-body">
				<?= $content; ?>
			</div>
		</div>
	</div><!-- content -->
<?php $this->endContent(); ?>