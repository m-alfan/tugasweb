<?php $this->widget('booster.widgets.TbNavbar',
    array(
        'type'  => 'inverse',
        'brand' => $config->name,
        'fixed' => TRUE,
        'fluid' => FALSE,
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
                'type'  => 'navbar',
                'activateParents' => true,
                'items' => array(
                   	array('label' => 'Home', 'url'     => array('/home/index')),
                    array('label' => 'Employee', 'url'    => array('/user/admin/admin')),
                    array('label' =>'Configuration of Web', 'url'=> array('/configWeb/update')),
                )
            ),
            array(
                'class'       => 'booster.widgets.TbMenu',
                'type'        => 'navbar',
                //'encodeLabel'    =>false,
                'htmlOptions' => array('class' => 'pull-right'),
                'items'       => array(
                    array('label' => 'Logout', 'url'=> array('/user/logout')),
                    array('label' => strtoupper(Yii::app()->user->name), 'url' => array('/user/profile/profile')),
                )
            )
        )
    )
);?>