<?php $this->beginContent('//layouts/main'); ?>
	<div class="col-md-3 column">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php
					$this->beginWidget('zii.widgets.CPortlet', array(
						'title'=>'<h4>Actions</h4>',
						'htmlOptions' => array('class' => 'nav-header')
					));

					$this->Widget('booster.widgets.TbMenu', array(
						'encodeLabel'    =>false,
						'items'=>$this->menu,
					));

					$this->endWidget();
				?>
			</div>
		</div>
	</div><!-- sidebar -->
	<div class="col-md-9 column">
		<div class="panel panel-default">
		  <div class="panel-body">
		    <?= $content; ?>
		  </div>
		</div>
	</div><!-- content -->

<?php $this->endContent(); ?>