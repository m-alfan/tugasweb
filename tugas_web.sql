CREATE TABLE IF NOT EXISTS `config_web` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `descr` text NOT NULL,
  `company` varchar(45) NOT NULL,
  `web` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `config_web` (`id`, `name`, `descr`, `company`, `web`) VALUES
(1, 'Tugas Web', 'Application', 'Pemrograman Web', 'www.muhamadalfan.com');


CREATE TABLE IF NOT EXISTS `profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `address` text,
  `no_telp` varchar(20) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `no_ktp` varchar(30) DEFAULT NULL,
  `jk` enum('m','f') DEFAULT NULL,
  `place_birth` varchar(30) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `npwp` varchar(20) DEFAULT NULL,
  `status_nikah` enum('s','m') DEFAULT NULL,
  `pendidikan` varchar(45) DEFAULT NULL,
  `jabatan` varchar(45) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;


INSERT INTO `profiles` (`user_id`, `name`, `address`, `no_telp`, `no_hp`, `no_ktp`, `jk`, `place_birth`, `date_birth`, `npwp`, `status_nikah`, `pendidikan`, `jabatan`, `date_start`, `date_end`, `photo`) VALUES
(1, 'Alfan', 'Jalan Raya Kalibata RT 001/007 No.26\r\nJakarta - Selatan', '085218390684', '085218390684', '', 'm', 'Jakarta', '1993-11-26', NULL, NULL, NULL, NULL, NULL, NULL, '2015-01-19-05-36-1.png'),
(2, 'Doang', 'alamat', 'telepon', 'handphone', 'nomor ktp', 'm', 'jakarta', '2015-01-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Alfan', 'Jalan Raya Kalibata', '', '085218390684', '', 'm', 'Jakarta', '2016-11-26', NULL, NULL, NULL, NULL, NULL, NULL, '2015-01-19-03-39-3.png');


CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `create_at`, `lastvisit_at`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 'be31802ecd4b11a27f7d0a5d80fe1eea', '2015-01-16 13:08:45', '2015-02-02 03:24:29', 1),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@demo.com', 'c2ffb59e6a078e8246dadee4eff4559a', '2015-01-16 13:08:45', '2015-01-21 16:37:39', 1),
(3, 'alfan', '3a492c0b90b1637377810ff5f02fa3e0', 'muhamad.alfan01@yahoo.com', '16175dc26b2673529a2c3335f99cf761', '2015-01-19 08:39:18', '0000-00-00 00:00:00', 1);

ALTER TABLE `profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;